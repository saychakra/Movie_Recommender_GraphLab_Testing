import pandas as pd
import numpy as np
import graphlab 

# Reading user files:
u_cols = ['user_id', 'age', 'sex', 'occupation', 'zip_code']
users = pd.read_csv('ml-100k/u.user', sep='|', names=u_cols, encoding='latin-1')

#Reading ratings file:
r_cols = ['user_id', 'movie_id', 'rating', 'unix_timestamp']
ratings = pd.read_csv('ml-100k/u.data', sep='\t', names=r_cols, encoding='latin-1')

#Reading items file:
i_cols = ['movie id', 'movie title' ,'release date','video release date', 'IMDb URL', 'unknown', 'Action', 'Adventure', 'Animation', 'Children\'s', 'Comedy', 'Crime', 'Documentary', 'Drama', 'Fantasy', 'Film-Noir', 'Horror', 'Musical', 'Mystery', 'Romance', 'Sci-Fi', 'Thriller', 'War', 'Western']
items = pd.read_csv('ml-100k/u.item', sep='|', names=i_cols, encoding='latin-1')

#dividing data into training and testing forms:
r_cols = ['user_id', 'movie_id', 'rating', 'unix_timestamp']
ratings_base = pd.read_csv('ml-100k/ua.base', sep='\t', names=r_cols, encoding='latin-1')
ratings_test = pd.read_csv('ml-100k/ua.test', sep='\t', names=r_cols, encoding='latin-1')

train_data = graphlab.SFrame(ratings_base)
test_data = graphlab.SFrame(ratings_test)

#creating a popularity model:
popularity_model = graphlab.popularity_recommender.create(train_data, user_id='user_id', item_id='movie_id', target='rating')

#using this to get the recommendation for 5 users and printing them :
# users = range(1,6) # specifies user id of first 5 users
popularity_recomm = popularity_model.recommend(users = range(1,6), k=5) # k specifies the limit of rank till which the values are allowed to be printed
popularity_recomm.print_rows(num_rows = 25)

# the thing to note here is that here all the movie_ids are in the same order for every user ie '1599, 1201, 1189, 814' because it is based on "popularity"
# this can be further verified by grouping the data according to highest mean recommendation

rating_group = ratings_base.groupby(by='movie_id')['rating'].mean().sort_values(ascending=False)
print(rating_group.head(20))
#this shows that all the movies have an average rating of 5. Which shows that the popularity approach is working

#Train Model
item_sim_model = graphlab.item_similarity_recommender.create(train_data, user_id='user_id', item_id='movie_id', target='rating', similarity_type='cosine')

#Make Recommendations:
item_sim_recomm = item_sim_model.recommend(users=range(1,6),k=5)
item_sim_recomm.print_rows(num_rows=25)